# Game Space Bar

## About this project
This is a student project created by students in the Centre for Digital Media.  
The game is written in C# using Unity 2018.3.41f1.  
Originally, the team used Unity team to collaborate.   
This repository is created after first complete prototype for future development.  

## Team Member

### Silver  
• Developer • User-Testing • Game Design • UX Design 

### Bas  
• 3D Artist • Animator • Game Design • Product Owner • Level Design • User-Testing

### Fernando  
• Project Manager • User-Testing • Level Design • Documentation 

### Wenyi  
• Sound Designer • Video • Level Design • User-Testing • Documentation

### Spencer  
• User-Testing • Game Design • UX Design • Music • Documentation


## Game Description
Space Bar is a 3D action/adventure game.  
It follows the story of a drunk alien that must grapple strategically through hazardous obstacles to avoid being consumed by a black hole.  
The player needs to use their mouse and keyboards to aim different kinds of objects in order to move around and collect items.  
The game’s free-roaming environment encourages players to explore new worlds that are rich in adventure.
