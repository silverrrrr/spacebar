﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonControl : MonoBehaviour {

    public Image gauge1;
    public Image gauge2;
    public Image gauge3;
    public RectTransform button;
    public blackholeForce force;

	
	// Update is called once per frame
	void Update () {
        GravityChange(force.gravityLevel);
		
	}
    void GravityChange(float gravityLevel){
        float amount = (gravityLevel / 120.0f) * 180.0f / 360;
        if(gravityLevel<=60){
            gauge1.fillAmount = amount;
            gauge2.fillAmount = 0;
            gauge3.fillAmount = 0;
        }
        else if(gravityLevel<=95){
            gauge1.fillAmount = 0.25f;
            gauge2.fillAmount = amount;
            gauge3.fillAmount = 0;
        }
        else{
            gauge1.fillAmount = 0.25f;
            gauge2.fillAmount = 95f / 120f * 180f / 360;
            gauge3.fillAmount = amount;
        }
        
        float buttonAngle = amount*360;
        button.localEulerAngles = new Vector3(0, 0, -buttonAngle);
    }
}
