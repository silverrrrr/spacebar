﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeLayer : MonoBehaviour {

    private int layer;

    private void OnTriggerEnter(Collider other)
    {
        layer = other.GetComponent<GameObject>().layer;
        other.GetComponent<GameObject>().layer = 3;

    }
    private void OnTriggerExit(Collider other)
    {
        other.GetComponent<GameObject>().layer = layer;
    }

}
