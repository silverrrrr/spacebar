﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class blackholeForce : MonoBehaviour {
    public float gravityLevel;
    public float gravityFactor=30;
    private Rigidbody rb;
    public GameObject planet;
    public Vector3 currentPosition;
    protected Vector3 planetPosition;
    public float losingCondition=120;

    private Animator anim;


    // Use this for initialization
    void Start () {

        rb = GetComponent<Rigidbody>();
        anim = GetComponent<Animator>();
        planetPosition = planet.transform.position;
        

        //InvokeRepeating("increaseGravity", 15.0f, 4.0f);
	}
	
	// Update is called once per frame
	void Update () {
       
        currentPosition = transform.position;
        Vector3 direction = currentPosition-planetPosition;
        float distance = Vector3.Distance(currentPosition, planetPosition);

        gravityLevel = distance - gravityFactor;
        rb.AddForce(direction.normalized*gravityLevel, ForceMode.Force);
        PlayerPrefs.SetFloat("gravity", gravityLevel);

        //if the distance from the planet is too far away, game fails
        if(gravityLevel>losingCondition){

            PlayerPrefs.SetString("goal", "Try Again");
            PlayerPrefs.SetInt("lose", 1);

            anim.Play("Heating");

            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }

       
	}

}
