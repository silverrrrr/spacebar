﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class grappling_line : MonoBehaviour {
    public Vector3 characterlocation;
    public Vector3 endlocation;
    private LineRenderer rope;
	// Use this for initialization
	void Start () {
        rope = GetComponent<LineRenderer>();
	}
	
	// Update is called once per frame
	void Update () {
        rope.transform.position = characterlocation;
        rope.SetPosition(1, (endlocation - characterlocation));
	}
}
