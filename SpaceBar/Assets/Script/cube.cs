﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class cube : MonoBehaviour {

    public float drunk;
    private float nextActionTime = 0.0f;
    public float period = 2.0f;
    //public GameObject alcohol;
    //private static readonly Random random = new Random();
    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        Vector3 p;

        if (Time.time > nextActionTime)
        {
            nextActionTime += period;
            // execute block of code here
            //each 2 seconds drunk level will decrease by the set time
            drunk--;
        }

        if (drunk > 0 && drunk < 80)
        {
            
            transform.Translate(Input.GetAxis("Horizontal") * 15.0f * Time.deltaTime, 0f, 0f);
            transform.Translate(0f, 0f, Input.GetAxis("Vertical") * 15.0f * Time.deltaTime);
            p = transform.position;
            //shake the character
            p.x += Mathf.Sin(Time.time * drunk) * (drunk*drunk / 500) * Time.deltaTime;
            transform.position = p;
        }
        if(drunk>=80){

            //the character will be out of control when the drunk level hits 80

            if (Mathf.Abs(Input.GetAxis("Horizontal"))>0){
                transform.Translate((Input.GetAxis("Horizontal")*2 + Random.Range(-10.0f, 10.0f)) * Time.deltaTime, 0f, Random.Range(-5.0f, 5.0f) * Time.deltaTime);
            }
            if (Mathf.Abs(Input.GetAxis("Vertical")) > 0)
            {
                transform.Translate(Random.Range(-5.0f, 5.0f)*Time.deltaTime, 0f, (Input.GetAxis("Vertical")*2 + Random.Range(-10.0f, 10.0f)) * Time.deltaTime);
            }

            p = transform.position;
            //shake the character
            p.x += Mathf.Sin(Time.time * drunk) * (drunk * drunk / 500) * Time.deltaTime;
            transform.position = p;
        }
        PlayerPrefs.SetFloat("drunk", drunk);
        
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag=="alcohol"){
            Destroy(other.gameObject);
            drunk += 15;
        }
    }
}
