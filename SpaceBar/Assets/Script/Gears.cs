﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Gears : MonoBehaviour {

    public Text gearValue;
    private int collectables;

    private GameObject player;

	// Use this for initialization
	void Start () {

        gearValue.text = "Gears"  + "0" + " / 5";
        player = GameObject.Find("Player");

    }
	
	// Update is called once per frame
	void Update () {

        collectables = player.GetComponent<Player>().collected;
        gearValue.text = "Gears " + collectables + " / 5"; ;

    }
}
