﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Tutorial : MonoBehaviour {
    public Image image1, image2, image3;
    public Button prev, next,back;
    [SerializeField]private int currentPage=0;

	// Use this for initialization
	void Start () {

	}
    private void Update()
    {
        if (currentPage == 0)
        {
            prev.enabled = false;
            prev.GetComponentInChildren<Text>().enabled = false;
            image1.enabled = true;
            image2.enabled = false;
            image3.enabled = false;
        }
        else if(currentPage==1){
            prev.enabled = true;
            next.enabled = true;
            next.GetComponentInChildren<Text>().enabled = true;
            prev.GetComponentInChildren<Text>().enabled = true;
            image1.enabled = false;
            image2.enabled = true;
            image3.enabled = false;
        }
        else if(currentPage==2){
            next.enabled = false;
            next.GetComponentInChildren<Text>().enabled = false;
            image1.enabled = false;
            image2.enabled = false;
            image3.enabled = true;
        }
    }

    public void PrevPage(){
        currentPage--;

    }
    public void NextPage(){
        currentPage++;
    }
    public void BackToMenu(){
        SceneManager.LoadScene("Menu");
    }
}
