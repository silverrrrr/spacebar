﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour {
    private const double V2 = 5.0;
    public RaycastHit hit;
    public Ray ray;

    public Vector3 objectPosition;
    public Vector3 currentPosition;
    public float speed = 30.0f;
    public float bottleSpeed = 10.0f;
    public float journeyLength;
    public int hitOnRock=10;
    //public float fracJourney; 

    public Animator anim;
    public MeshRenderer avatar;
    public Renderer rend;
    public Material mat;

    public float hurtTimer = 2.0f;
    protected float countdown;
    public float speedH = 2.0f;
    public float speedV = 2.0f;
    [SerializeField]private float yaw = 0.0f;    
    [SerializeField]private float pitch = 1.0f;
    protected GameObject hitObject=null;

    public GameObject playerObject;

    private Color32 tempColor;
    public int range=40;
    public int explosionImpact=1000;

   

    public GameObject hook;
    public GameObject hookholder;
    private ParticleSystem ps;
    private Rigidbody Rigidbody;
    public blackholeForce Blackhole;
    public AudioSource drinking, exploding, hitting, goaled,powerup,collecting;
    


    protected bool isfired=false;
    protected bool ishurt = false;
    public int collected;
    public int collectForGoal=2;
    public Vector3 centers;

    // Use this for initialization
    private void Awake()
    {
        StopAllCoroutines();
        if (PlayerPrefs.GetInt("lose")==1 && PlayerPrefs.GetInt("isSaved")==1){
            float x = PlayerPrefs.GetFloat("positionx");
            float y = PlayerPrefs.GetFloat("positiony");
            float z = PlayerPrefs.GetFloat("positionz");

                transform.position = new Vector3(x, y, z);
                collected = PlayerPrefs.GetInt("collected") - 1;
            


            PlayerPrefs.SetInt("lose", 0);
        }
        else{
            PlayerPrefs.DeleteAll();

        }
        mat.color = Color.black;

    }
    void Start()
    {
        countdown = hurtTimer;
        Rigidbody = this.GetComponent<Rigidbody>();

        avatar = GetComponent<MeshRenderer>();
        anim = GetComponent<Animator>();
        rend = GetComponent<Renderer>();
        //mat = GetComponent<Material>();
       
    }

    IEnumerator PowerUp()
    {
        speed = speed + 100;
        range = range + 30;
        powerup.Play();
        yield return new WaitForSeconds(8);
        mat.color = Color.black;
        speed = 30.0f;
        range = 40;
        powerup.Stop();
        ResetGun();
        yield return null;

    }
    IEnumerator ReachGoal(){
        goaled.Play();
        yield return new WaitForSeconds(3);
        SceneManager.LoadScene("Ending");
        yield return null;
    }

    // Update is called once per frame
    void Update () {

        //anim.Play("Idle");

        RotatingWithMouse();
        currentPosition = gameObject.transform.position;
        if (!ishurt)
        {

               

           ray = Camera.main.ScreenPointToRay(Input.mousePosition);
           


            Debug.DrawRay(ray.origin, ray.direction * 1000, Color.yellow);


            //ray = Camera.main.ScreenPointToRay(Camera.main.transform.forward);
            // Debug.DrawRay(currentPosition, Input.mousePosition, Color.red);

            //if (Physics.Raycast(Camera.main.transform.position,Camera.main.transform.forward, out hit, range))
            //{
            if (Physics.Raycast(ray, out hit, range,1, QueryTriggerInteraction.Ignore))
            {
                //PlayerPrefs.SetString("goal", "");
                

                //when the 
                Debug.Log(hit.collider.gameObject.name);
                if (hitObject == null)
                {

                    hitObject = hit.collider.gameObject;
                    centers = hitObject.GetComponent<BoxCollider>().transform.position;
                    

                    tempColor = hitObject.GetComponent<MeshRenderer>().material.color;
                    //Debug.Log(hitObject.name);
                    hitObject.GetComponent<MeshRenderer>().material.color = new Color32(0, 0, 255, 255);
                    PlayerPrefs.SetInt("onObject", 1);
                }
                else if (hitObject != null && hitObject!=hit.collider.gameObject)
                {
                   

                    Debug.Log(hit.transform.position);
                    hitObject.GetComponent<MeshRenderer>().material.color = tempColor;
                    hitObject = null;
                    PlayerPrefs.SetInt("onObject", 0);

                }


                if (Input.GetKey(KeyCode.E) || Input.GetKey(KeyCode.Mouse0))
                {
                    PlayerPrefs.SetString("goal", "");

                    // objectPosition = hit.transform.position + centers;
                    objectPosition = centers;

                   
                    hook.transform.position = hookholder.transform.position;
                    Vector3 direction = (currentPosition - objectPosition);
                    journeyLength=Vector3.Distance(currentPosition, objectPosition);

                    //if the object is a drink bottle, it fly to player and hit player
                    if (hit.collider.tag == "Alcohol")
                    {
                        //hit.transform.position= Vector3.Lerp( objectPosition, currentPosition, fracJourney);
                        
                       // hit.rigidbody.AddForce(direction *bottleSpeed * Time.deltaTime,ForceMode.Acceleration);
                        hit.rigidbody.AddForceAtPosition(direction.normalized*5, currentPosition,ForceMode.Impulse);
                        objectPosition = Vector3.zero;
                        ResetGun();
                    }
                    if (hit.collider.tag == "PowerUp")
                    {
                        playerObject.GetComponent<MeshRenderer>().material.color = new Color32(255, 0, 0, 255);
                    }
                        //if the object is barrel then it explode and hit player
                    if (hit.collider.tag=="BadBarrel"){
                        ps = hit.collider.GetComponent<ParticleSystem>();

                            var emission = ps.emission;
                            var main = ps.main;
                            emission.enabled = true;
                            main.loop = false;
                            MeshRenderer mr;
                            mr = hit.collider.GetComponent<MeshRenderer>();
                            mr.enabled = false;
                            objectPosition = Vector3.zero;
                            ResetGun();
                        Rigidbody.AddForce(direction/journeyLength*explosionImpact, ForceMode.Impulse);
                        direction = Vector3.zero;
                            
                            ishurt = true;
                            PlayerPrefs.SetString("goal", "Explosion!");
                        anim.Play("Explode");
                        exploding.Play();

                    }


                    //journeyLength = Vector3.Distance(currentPosition, objectPosition);
                    //fracJourney = (speed * Time.deltaTime) / journeyLength;


                }
               
            }

            else if (Input.GetKey(KeyCode.E) || Input.GetKey(KeyCode.Mouse0))
            {
                PlayerPrefs.SetString("goal", "No object in the gun range");

            }
            else if(hitObject!=null)
            {
                hitObject.GetComponent<MeshRenderer>().material.color = tempColor;
                hitObject = null;
                PlayerPrefs.SetInt("onObject", 0);
            }

            if (!objectPosition.Equals(Vector3.zero)&&journeyLength>1){
                firehook(objectPosition, speed * 4);

                anim.Play("Reaching");
                //anim.Play("Idle");
                //anim.SetTrigger("Reach");
                
                //gameObject.transform.position = Vector3.Lerp(currentPosition, objectPosition, fracJourney);
                gameObject.transform.position = Vector3.MoveTowards(currentPosition, objectPosition, speed * Time.deltaTime);



            }



        }
        else if(hurtTimer <= 0.0f){
           
            ishurt = false;
            hurtTimer = countdown;

        }
        else{
           
            hurtTimer = hurtTimer - Time.deltaTime;


        }

        if(Input.GetKey(KeyCode.Space) || Input.GetKey(KeyCode.Mouse1)){
            objectPosition = Vector3.zero;
            ResetGun();
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        
        objectPosition = Vector3.zero;
        ResetGun();
        //if alcohol obstacles hit player, it hurts player
        if (collision.collider.tag=="Alcohol"){

           // Rigidbody rig;
            //ishurt = true;
            //this.GetComponent<Rigidbody>().Sleep();


            PlayerPrefs.SetString("goal", "It hurts!");

            Destroy(collision.gameObject);


        }
        if(collision.collider.tag=="Rock"){
            Blackhole.gravityFactor -= hitOnRock;
            Debug.Log("Ah");
            hitting.Play();
        }

        if (collision.collider.tag=="PowerUp")
        {
            

            StartCoroutine(PowerUp());

            Destroy(collision.gameObject);
            mat.color = Color.red;



            Debug.Log("This works!");

        }

        //if alcohol obstacles hit player, it hurts player
        if (collision.collider.tag == "Collectable")
        {
            collected++;
            collecting.Play();

            PlayerPrefs.SetInt("collected", collected);
            PlayerPrefs.SetFloat("positionx", transform.position.x);
            PlayerPrefs.SetFloat("positiony", transform.position.y);
            PlayerPrefs.SetFloat("positionz", transform.position.z);
            PlayerPrefs.SetInt(collision.collider.name, 1);
            Destroy(collision.gameObject);
            PlayerPrefs.SetInt("isSaved", 1);
        }
        //if player gets to the goal with enough parts collected, win
        if (collision.collider.tag=="Goal"){


            if(collected>=collectForGoal){
                PlayerPrefs.SetString("goal", "You Reached Goal!");
                Destroy(collision.gameObject);
                PlayerPrefs.SetFloat("GoalTime", Time.time);
                StartCoroutine(ReachGoal());

            }
            else{
                PlayerPrefs.SetString("goal", "Parts Not Enough");
            }


        }
        if (collision.collider.tag == "GoodBarrel")
        {
            drinking.Play();
            Destroy(collision.gameObject);
            Blackhole.gravityFactor += 10;
        }
        if(collision.collider.tag=="BadBarrel"){
            ps = collision.collider.GetComponent<ParticleSystem>();

            var emission = ps.emission;
            var main = ps.main;
            emission.enabled = true;
            main.loop = false;
            MeshRenderer mr;
            mr = collision.collider.GetComponent<MeshRenderer>();
            mr.enabled = false;
            objectPosition = Vector3.zero;
            ResetGun();
            Rigidbody.AddForce(Vector3.back* explosionImpact, ForceMode.Impulse);


            ishurt = true;
            PlayerPrefs.SetString("goal", "Explosion!");
            anim.Play("Explode");
            exploding.Play();
        }


    }


    private void WaitForSeconds(float v1, double v2)
    {
        throw new NotImplementedException();
    }

    private void RotatingWithMouse(){


        //use mouse to adujst camera
        //yaw += speedH  *Time.deltaTime* Input.GetAxis("Mouse X");
        //pitch -= speedV  *Time.deltaTime* Input.GetAxis("Mouse Y");

        //use wasd or arrow key to adjust camera
        yaw += speedH  *Time.deltaTime* Input.GetAxis("Horizontal");
        pitch -= speedV  *Time.deltaTime* Input.GetAxis("Vertical");
        

            transform.eulerAngles=new Vector3(pitch ,yaw , 0.0f);

        
        //Camera.main.transform.Rotate(speedV * Time.deltaTime * Input.GetAxis("Mouse Y"), 0f, 0f);
        //Camera.main.transform.RotateAround(this.transform.position, Vector3.down,speedH *Time.deltaTime * Input.GetAxis("Mouse X"));
        //Camera.main.transform.RotateAround(this.transform.position, Vector3.left, speedV *Time.deltaTime * Input.GetAxis("Mouse Y"));


        // Camera.main.transform.RotateAround(this.transform.position, Vector3.forward, speedV * Time.deltaTime * Input.GetAxis("Mouse Y"));

        if (Input.GetKey(KeyCode.R) )
        {
            yaw = 0;
            pitch = 0;
            transform.eulerAngles = new Vector3(pitch, yaw, 0.0f);
        }
       
    }
    private void firehook(Vector3 position,float hookspeed){
       
        LineRenderer rope = hook.GetComponent<LineRenderer>();

        if(ishurt){
            position = Vector3.zero;
            rope.SetPosition(1, hookholder.transform.position);
        }

        hook.transform.position = Vector3.MoveTowards(hook.transform.position, position, hookspeed * Time.deltaTime);
        rope.SetPosition(0, hookholder.transform.position);
        rope.SetPosition(1, hook.transform.position);

       

    }
    private void ResetGun(){

        hook.transform.position = hookholder.transform.position;
        LineRenderer rope = hook.GetComponent<LineRenderer>();
        rope.SetPosition(0, Vector3.zero);
        rope.SetPosition(1, Vector3.zero);
    }
}
