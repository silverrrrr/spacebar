﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class character : MonoBehaviour {
    //position of the goal object
    public Vector3 position;
    private float journeyLength;
    private float range=8.0f;
    //private int mode;
    public float speed = 5.0f;
    public bool notOnCollision = true;
    public bool isshow = false;
    public grappling_line sendLocation;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        Vector3 objectLocation;

        objectLocation = gameObject.transform.position;
        if(Input.GetKey("1")){
            range = 5.0f;
            speed = 8.0f;
        }
        if(Input.GetKey("2")){
            range = 8.0f;
            speed = 5.0f;
        }
        if (!position.Equals(Vector3.zero))
        {
            journeyLength = Vector3.Distance(objectLocation, position);
            Debug.Log("journey length is" + journeyLength);
            if(journeyLength<range){
            Debug.DrawLine(objectLocation, position, Color.black, 0, false);
            //send the current location and the object location to line renderer
            sendLocation.characterlocation = objectLocation;
            sendLocation.endlocation = position;
            float fracJourney = (speed * Time.deltaTime)/journeyLength;

                //when hit space the object pulled
                if (Input.GetKey("space"))
                {
                    //gameObject.transform.position = Vector3.Lerp(objectLocation, position, fracJourney);
                    //gameObject.transform.position = Vector3.MoveTowards(objectLocation, position, 10);
                    if (isshow == false)
                    {
                        PlayerPrefs.SetString("goal", "");
                        isshow = true;
                    }
                }
            }
            else{
                sendLocation.characterlocation= new Vector3(0f,0f,0f);
                sendLocation.endlocation = new Vector3(0f, 0f, 0f);
            }
            
                
            //}
        }
	}
    private void OnCollisionEnter(Collision collision)
    {
        notOnCollision = false;
    }
    //private void OnCollisionExit(Collision collision)
    //{
    //    notOnCollision = true;
    //}
}
