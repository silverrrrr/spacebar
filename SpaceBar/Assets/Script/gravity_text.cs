﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class gravity_text : MonoBehaviour {
    public string gravity;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        GetComponent<Text>().text = "Gravity Level:" + PlayerPrefs.GetFloat(gravity) + "";
    }
}
