﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lava : MonoBehaviour {
    public blackholeForce Blackhole;
    public float factor;
   
    private void OnTriggerStay(Collider other)
    {
        
        if(other.tag=="Hook"){
            factor = Blackhole.gravityFactor;
            Blackhole.gravityFactor -= 5*Time.deltaTime;
            Debug.Log("Lava working");
        }

    }

   
    private void OnTriggerExit(Collider other)
    {
        Blackhole.gravityFactor = factor-1;
    }
}
