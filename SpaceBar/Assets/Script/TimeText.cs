﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimeText : MonoBehaviour {
    private float minutes, seconds, times;
	// Use this for initialization
	void Start () {
        times = PlayerPrefs.GetFloat("GoalTime");
        minutes = (int)(times / 60f);
        seconds = (int)(times % 60f);
        this.GetComponent<Text>().text+="\n"+ minutes.ToString("00") + ":" + seconds.ToString("00");
    }
   

}
