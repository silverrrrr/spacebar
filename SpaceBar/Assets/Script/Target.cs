﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Target : MonoBehaviour {
    public string onObject;
    private Image temp;
    public Vector3 tempScale;
    // Use this for initialization
    void Start () {
        Cursor.visible = false;
        temp = this.GetComponent<Image>();
        tempScale = transform.localScale;
    }
	
	// Update is called once per frame
	void Update () {
        transform.position = Input.mousePosition;
        if(PlayerPrefs.GetInt(onObject)==1){
            transform.localScale = new Vector3(0.3f, 0.3f, 0f);
            this.GetComponent<Image>().color = new Color(253f, 14f, 14f);

        }
        else{
            transform.localScale = tempScale;
            this.GetComponent<Image>().Equals(temp);
        }
    }
}
