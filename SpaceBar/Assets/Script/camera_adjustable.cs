﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class camera_adjustable : MonoBehaviour {

    public Transform target;
    public int relativeHeight = 10;
    public float changeposition;
    public int zDistance = 1;
    public int dampSpeed = 1;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        Vector3 newPos = target.position + new Vector3(changeposition*Time.deltaTime, relativeHeight, -zDistance);
        transform.position = Vector3.Lerp(transform.position, newPos, Time.deltaTime * dampSpeed);

        if (Input.GetKey("left"))
        {
            changeposition += 1f;

            transform.Rotate(0f, -5.0f * Time.deltaTime, 0f);
        }
        if (Input.GetKey("right"))
        {
            changeposition -= 1f;

            transform.Rotate(0f, 5.0f * Time.deltaTime, 0.0f);
        }
       
    }

}
