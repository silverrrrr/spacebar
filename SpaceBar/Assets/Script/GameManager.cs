﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

    public Text time;
    private float seconds,minutes;

	// Use this for initialization
	void Start () {
        GameObject[] barrels=GameObject.FindGameObjectsWithTag("Barrel");
        GameObject[] alcohols = GameObject.FindGameObjectsWithTag("Alcohol");
        GameObject[] collects = GameObject.FindGameObjectsWithTag("Collectable");
        int goodbarrel = 0;
        foreach (GameObject barrel in barrels)
        {
            if (Random.Range(1, 10) > 3)
            {
                barrel.tag = "GoodBarrel";
                goodbarrel++;
               
            }
            else{
                barrel.tag = "BadBarrel";
            }
        }
        Debug.Log("Goodbarrel" + goodbarrel);
        foreach (GameObject alcohol in alcohols)
        {
            if (Random.Range(1, 10) > 4)
            {
                alcohol.GetComponent<MeshRenderer>().enabled = false;


            }
           
        }
        foreach(GameObject collect in collects){
            if(PlayerPrefs.HasKey(collect.name)){
                Destroy(collect);
            }
        }
    }
	
	// Update is called once per frame
	void Update () {
        minutes = (int)(Time.time / 60f);
        seconds = (int)(Time.time % 60f);
        time.text = "Time:" + minutes.ToString("00") + ":" + seconds.ToString("00");
	}
}
