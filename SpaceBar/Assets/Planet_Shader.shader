﻿Shader "Custom/Planet" {
    Properties {
        _Color("Color", Color) = (1, 1, 1, 1)
        _MainTex("Albedo", 2D) = "white" {}
        _EmissiveTex("Emissive", 2D) = "black" {}
        _NegativeEmissiveColor("EmissiveColor", Color) = (0, 0, 0, 0)
        _EmissivePower ("EmissivePower", Range (0,0.5)) = 0
        
    }
    SubShader {
        Tags {
            "RenderType" = "Opaque"
        }
        LOD 200

        CGPROGRAM
        #pragma surface surf CelShadingForward
        #pragma target 3.0

        half4 LightingCelShadingForward(SurfaceOutput s, half3 lightDir, half atten) {
            half NdotL = dot(s.Normal, lightDir) * 0.5 + 0.5;
            if (NdotL <= 0.0) NdotL = 0;
            else NdotL = 1;
            half4 c;
            
            c.rgb = s.Albedo * _LightColor0.rgb * (NdotL * atten * 2);
            c.a = s.Alpha;
            return c;
        }

        sampler2D _MainTex;
        fixed4 _Color;
        fixed4 _NegativeEmissiveColor;
        half _EmissivePower;
        sampler2D _EmissiveTex;

        struct Input {
            float2 uv_MainTex;
            float2 uv_EmissiveTex;
        };

        void surf(Input IN, inout SurfaceOutput o) {
            // Albedo comes from a texture tinted by color
            fixed4 e = tex2D(_EmissiveTex, IN.uv_EmissiveTex);
            fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;
            fixed4 d = lerp(_NegativeEmissiveColor.rgba, e.rgba, _EmissivePower);
            fixed4 s = d + c;
            o.Albedo = s.rgb;
            o.Alpha = s.a;
        }
        ENDCG
    }
}