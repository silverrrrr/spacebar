﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraLayer : MonoBehaviour {
    private int layer;

   
    private void OnTriggerEnter(Collider other)
    {
         
        layer = other.gameObject.layer;
        other.gameObject.layer = 3;
    }
    private void OnTriggerExit(Collider other)
    {

        other.gameObject.layer = layer;
    }
}
