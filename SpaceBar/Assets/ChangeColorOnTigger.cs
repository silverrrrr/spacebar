﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeColorOnTigger : MonoBehaviour {

   
    private void OnTriggerStay(Collider other)
    {
        if(other.tag=="BadBarrel")
        {

            other.GetComponent<MeshRenderer>().material.color += new Color(Time.deltaTime, 0f, 0f);
        }

    }
    private void OnTriggerExit(Collider other)
    {
        other.GetComponent<MeshRenderer>().material.color = Color.black;
    }
}
