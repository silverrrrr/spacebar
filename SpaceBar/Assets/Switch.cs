﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Switch : MonoBehaviour {
    public Material lava;
    public GameObject planet;
    public GameObject[] lavaColliders;
    

    private void Start()
    {
        lava = planet.GetComponent<MeshRenderer>().material;
        lavaColliders = GameObject.FindGameObjectsWithTag("Lava");
        
    }

    IEnumerator TurnOffLava(){
        lava.SetFloat("_EmissivePower", 0f);
        foreach(GameObject lavaCollider in lavaColliders){
            lavaCollider.GetComponent<CapsuleCollider>().enabled = false;

        }
        //switch on


        yield return new WaitForSeconds(5);

        lava.SetFloat("_EmissivePower", 0.5f);
        foreach (GameObject lavaCollider in lavaColliders)
        {
            lavaCollider.GetComponent<CapsuleCollider>().enabled = true;
        }

        yield return null;

    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.tag == "Player" && !collision.collider.isTrigger)
        {

            StartCoroutine(TurnOffLava());
            Debug.Log("Switch on");

        }
    }
    

 

}
